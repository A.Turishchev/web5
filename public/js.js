function onClick() {
    let num1 = document.getElementById("num1").value;
    let num2 = document.getElementById("num2").value;
    console.log(num1, num2);
    
    let result = document.getElementById("result");
    
    if((num1.match(/^[0-9]*[.]?[0-9]+$/) === null) || (num2.match(/^[0-9]*[.]?[0-9]+$/) === null)){
        result.innerHTML = "Ошибка";
        alert("Введите числа > 0");
    }
    else {
        result.innerHTML = "Стоимость заказа: " + (num1*num2);
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button");
    b.addEventListener("click", onClick);
});

